## Histoire
**En tant que** {persona ayant un besoin}

**Je peux** {action répondant au besoin}

**Afin de** {description du besoin}

## Degré de pertinence du besoin

- [ ] **le besoin** émane d'un des concernés
- [ ] ou d'un membre de l'équipe qui n'a pas lui même ce besoin ?

---
- [ ] **l'action** pour répondre au besoin émane d'un des concernés
- [ ] ou d'un membre de l'équipe qui n'a pas lui même ce besoin ?

## Suggestions ergonomiques

* Exemples visuels de la fonctionnalité (copier des url, ou télécharger des fichiers en pj)
* Comment activer cette fonctionnalité sur l'application ?

## Etapes de réalisation

- [ ] {etape_de_réalisation}
- [ ] {etape_de_réalisation}
- [ ] {etape_de_réalisation}
- [ ] {etape_de_réalisation}

---
- [ ] validation **technique** par l'équipe (ça fonctionne ?)
- [ ] validation **ergonomique** par l'équipe (c'est pratique ?)
- [ ] validation **editoriale** par l'équipe (pas de fautes ? quelle que soit la langue ? et les messages d'erreurs ?)
- [ ] validation par l'émetteur du besoin

## Contact
*Pour pouvoir demander plus de précision / faire d'autres propositions pour répondre au besoin.*

Exemples :
- **Demandeur/commanditaire** John Doe jd@ici.fr 0123456789 http://facebook.com/johndoe

Exemples de statuts : *en quelle qualité est-il pertinant de les contacter ?*
- Journaliste
- Militant technophile
- Militant techno-frileux
- Traducteur

---
- **{statuts}** {prénom nom} {email} {téléphone} {autre}
- **{statuts}** {prénom nom} {email} {téléphone} {autre}
- **{statuts}** {prénom nom} {email} {téléphone} {autre}
