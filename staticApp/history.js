define(['./smartEvents','./trad/tradRenderer'], (ev,tradR) => {
	const t = tradR.t;
	const titleTemplate = {};
	let fullGraph, config;
	function reset() {

	}
	function init() {
		ev.after(['history.fullGraph.ok','history.config.ok'],()=>setTitleFrom(config));
		ev.need('fullGraph',(fG)=> { fullGraph = fG; ev.send('history.fullGraph.ok'); });
		ev.need('config',(cfg)=> { config = cfg; ev.send('history.config.ok'); });
		ev.on('config.selected change', (selected)=>saveStateInHistory(selected) ); // aller chercher le label
		ev.on('config.userMode change', (selected)=>saveStateInHistory(selected) );
		// selected < mode < titre site ou mode:selected avec mode en une seule lettre.
		//ev.on('config.search change', (selected)=>saveStateInHistory(selected) );
		// ne mémoriser la recherche qu'au changement de mode, pas à chaque saisie de caractère durant la frappe.
		//TODO: title traduit et composé d'une partie fixe et d'une partie variable également userfriendly.
	}
	titleTemplate.trad = (config)=> t('windowTitleUserMode_'+config.userMode)+t('windowTitleMainTitle');
	titleTemplate.viz = (config)=> t('windowTitleUserMode_'+config.userMode)+formatSelectedForTitle(config.selected)+t('windowTitleMainTitle');
	titleTemplate.edit = (config)=> t('windowTitleUserMode_'+config.userMode)+formatSelectedForTitle(config.selected)+t('windowTitleMainTitle');
	function saveStateInHistory(config) {
		history.pushState({}, setTitleFrom(config), location.hash);
	}
	function setTitleFrom(config){
		const title = titleTemplate[config.userMode](config);
		document.title = title;
		return title;
	}
	function formatSelectedForTitle(id){
		if(id) {
			const type = id.split('-')[0];
			const selected = fullGraph[type].find((item)=>item.id === id );
			if(selected && selected.label) return t(selected.label)+t('windowTitleSeparator');
			else return t(id)+t('windowTitleSeparator');
		}
		else return '';
	}

	return {
		reset,
		init
	}
});
