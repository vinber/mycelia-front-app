define([
	'./smartEvents',
	'./htmlTools'
], (ev, htmlTools) => {
	const buildNode = htmlTools.buildNode;
	let toolsContainer;
	let config = {};
	function init() {
		ev.need('config',async (cfg)=>{
			config = cfg;
			toolsContainer = document.getElementById("advanced-tools");//config.legendAreaId);
			await renderAdvTools();
			ev.send('advToolsView.ready');
		});

	}
	async function renderAdvTools(){
		if(config.persistanceAPI){
			const whatCanIDoUrl = config.persistanceAPI+'/auth/user-info';
			const access = JSON.parse(await (await fetch(whatCanIDoUrl)).text());
			console.log(access);

			//const authUrl = config.persistanceAPI+'auth/email/mycelia-admin@yopmail.com/'+atob(window.location.href);
			/*
			si authentifier : lister les droit, lien sauvegarder/publier, lien déconnecter
			sinon : s'authentifier
			 */
		}
		const login = buildNode("#loginButton.pictoButton");
		login.addEventListener('click',async ()=>{
			const email = prompt("Merci d'indiquer votre email pour recevoir votre lien d'authentification.", "mycelia-admin@yopmail.com");
			const authUrl = config.persistanceAPI+'/auth/email/'+email+'/'+btoa(window.location.href);
			console.log(authUrl);
			await fetch(authUrl);
			alert("email d'authentificaiton envoyé");
		});
		toolsContainer.appendChild(login);
	}

	return {
		init
	}
});
